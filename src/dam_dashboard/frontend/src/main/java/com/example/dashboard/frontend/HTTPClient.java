package com.example.dashboard.frontend;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.client.WebClient;

public class HTTPClient extends AbstractVerticle {
	
	public static JsonArray jsa;
	private Runnable runner;
	
	public HTTPClient(Runnable runner) {
		HTTPClient.jsa = new JsonArray();
		this.runner = runner;
	}
	
	public void start() throws Exception {		
	
		String host = "localhost";
		int port = 8080;

		Vertx vertx = Vertx.vertx();
		WebClient client = WebClient.create(vertx);

		new Thread(() ->  {
			while(true) {
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				client
				  .get(port, host, "/api/data")
				  .send()
				  .onSuccess(res -> {
					  HTTPClient.jsa = res.bodyAsJsonObject().getJsonArray("data");
					  this.runner.run();
				  })
				  .onFailure(err -> {
					  
				  });
			}
		}).start();
		
	}
	
}
