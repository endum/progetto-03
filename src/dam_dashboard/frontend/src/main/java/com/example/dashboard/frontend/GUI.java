package com.example.dashboard.frontend;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;

import java.awt.Color;

import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.SwingConstants;


public class GUI {

	private JFrame frame;
	private JPanel mainPanel;
	private JLabel lblState;
	private JLabel lblDam;
	private DynamicTimeSeriesChart water;
	
	public GUI() {
		initialize();
		this.frame.setVisible(true);
	}
	
	public void hideWater() {
		this.water.setVisible(false);
	}
	
	public void showWater() {
		this.water.setVisible(true);
	}
	
	public void updateWater(int high) {
		this.water.update(high);
	}
	
	public void hideDam() {
		this.lblDam.setVisible(false);
	}
	
	public void showDam() {
		this.lblDam.setVisible(true);
	}
	
	public void setDam(int opening) {
		this.lblDam.setText("Dam opening: " + opening);
	}
	
	public void setState(int state, int manual) {
		if(manual == 1) {
			this.lblState.setText("State: " + itost(state) + ", on MANUAL mode");
		} else {
			this.lblState.setText("State: " + itost(state) + ", on AUTOMATIC mode");
		}
	}
	
	private String itost(int state) {
		if(state == 0) {
			return "NORMAL";
		}
		if(state == 1) {
			return "PRE-ALLARM";
		}
		return "ALLARM";
	}
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(new Rectangle(800,570));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		lblState = new JLabel("State: ");
		lblState.setHorizontalAlignment(SwingConstants.LEFT);
		lblState.setFont(new Font("Cambria", Font.BOLD, 15));
		lblState.setBackground(new Color(153, 153, 255));
		
		lblDam = new JLabel("Dam opening: ");
		lblDam.setHorizontalAlignment(SwingConstants.LEFT);
		lblDam.setFont(new Font("Cambria", Font.BOLD, 15));
		lblDam.setBackground(new Color(153, 153, 255));
		
		water = new DynamicTimeSeriesChart("Water level");
		
		mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(250, 500));
		mainPanel.setLayout(new BorderLayout(0, 0));
		frame.getContentPane().add(mainPanel);
		
		mainPanel.add(lblState, BorderLayout.NORTH);
		mainPanel.add(lblDam, BorderLayout.CENTER);
		mainPanel.add(water, BorderLayout.EAST);
	}

}
