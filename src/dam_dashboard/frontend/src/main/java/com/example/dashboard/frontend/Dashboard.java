package com.example.dashboard.frontend;

import javax.swing.SwingUtilities;

import io.vertx.core.json.JsonArray;

public class Dashboard {

	public static void main(String[] args) throws Exception {
		
		final int MAX_HIGH = 500;
		
		GUI gui = new GUI();
		
		new HTTPClient(()->{
			try {
				Thread.sleep(300);
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						JsonArray jsa = HTTPClient.jsa;
						gui.updateWater((int)(MAX_HIGH - (jsa.getJsonObject(1).getDouble("value") * 100)));
						gui.setState(jsa.getJsonObject(0).getInteger("state"),
									 jsa.getJsonObject(0).getInteger("manual"));
						gui.setDam(jsa.getJsonObject(0).getInteger("opening"));
						switch(jsa.getJsonObject(0).getInteger("state")) {
						case 0:
							gui.hideWater();
							gui.hideDam();
							break;
						case 1:
							gui.showWater();
							gui.hideDam();
							break;
						case 2:
							gui.showWater();
							gui.showDam();
							break;
						}
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		
	}

}
