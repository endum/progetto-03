#ifndef __SERIALBT__
#define __SERIALBT__
#include <Arduino.h>

#include "SoftwareSerial.h"
#include <Wire.h>

class SerialBT {
public:
  SerialBT(int rate, int RX, int TX);
  void out(String msg);
  String in();
  bool present();
private:
  SoftwareSerial* BTChannel;
};

#endif
