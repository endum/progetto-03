#include <Arduino.h>
#include "SerialBT.h"

SerialBT::SerialBT(int rate, int RX, int TX){
  this->BTChannel = new SoftwareSerial(RX, TX);
  this->BTChannel->begin(rate);
}

void SerialBT::out(String msg){
  this->BTChannel->println(msg);
}
String SerialBT::in(){
  if (this->BTChannel->available()){
    return this->BTChannel->readStringUntil('\n');
  }
  return "!";
}
bool SerialBT::present(){
  return this->BTChannel->available();
}
