#include <Arduino.h>
#include "SerialCable.h"

SerialCable::SerialCable(int rate){
  Serial.begin(rate);
  while (!Serial){}
}

void SerialCable::out(String msg){
  Serial.println(msg);
}
String SerialCable::in(){
  if (Serial.available()){
    return Serial.readStringUntil('\n');
  }
  return "!";
}
bool SerialCable::present(){
  return Serial.available();
}
