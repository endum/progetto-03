#include <Arduino.h>
#include "Dam.h"

Dam::Dam(int pin){
  this->pin = pin;
}

void Dam::initDevice(){
  this->myservo.attach(this->pin);
}
void Dam::setOpening(int opening){
  this->myservo.write(map(opening, 0, 100, 10, 160));  // Non da 0 a 180 perchè altrimenti il servo vibra (non riesce a giungere a fine corsa).
}
