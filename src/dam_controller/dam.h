#ifndef __DAM__
#define __DAM__
#include <Arduino.h>

#include <Servo.h>

class Dam {
public:
  Dam(int pin);
  void initDevice();
  void setOpening(int opening);
private:
  Servo myservo;
  int pin;
};

#endif
