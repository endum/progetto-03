#include "Led.h"
#include "SerialCable.h"
#include "SerialBT.h"
#include "Dam.h"

#define RX 2      // Da connettere a TX del modulo BT.
#define TX 3      // Da connettere a RX del modulo BT.
#define LED 13    // Pin del led.
#define RATE 9600 // Baudrate per le comunicazioni seriali.
#define DAMp 9     // Pin del servo.

Led* led;
SerialCable* sc;
SerialBT* sb;
Dam* dam;

int state;
int pSC;
int pSB;
int p;
bool wantManual;
bool manual;

unsigned long blinkLed;

void setup() {
  led = new Led(LED);
  led->initDevice();

  sc = new SerialCable(RATE);

  sb = new SerialBT(RATE, RX, TX);

  dam = new Dam(DAMp);
  dam->initDevice();

  state = 0;
  pSC = 0;
  pSB = 0;
  p = 0;
  wantManual = 0;
  manual = 0;

  blinkLed = millis();

  sc->out("0 0");
}

void loop() {
  if(sc->present()){  // Se ricevo dati dal DS.
    String msg = sc->in();
    char operation = msg[0];
    String content = msg.substring(2); 
    switch(operation){
      case '0':   // NUOVO STATO.
        state = content.toInt();
      break;
      case '1':   // APERTURA DIGA.
        pSC = content.toInt();
      break;
    }
  }

  if(sb->present()){  // Se ricevo qualcosa dal DM.
    String msg = sb->in();
    char operation = msg[0];
    String content = msg.substring(2); 
    switch(operation){
      case '0':   // MANUALE.
        wantManual = content.toInt();
      break;
      case '1':   // APERTURA DIGA.
        pSB = content.toInt();
      break;
    }
  }

  switch(state){
    case 0:     // NORMALE.
      pSC = 0; pSB = 0; // Per evitare scostamenti nel cambio di stato.
      p = 0;    // La mancanza di break è voluta.
    case 1:     // PREALLARME.
      if(manual){
        manual = 0;
        wantManual = 0;
        sc->out("0 0");
      }
      led->turnOff();
    break;
    case 2:     // ALLARME.
      switch(manual){
        case 0:   // Automatico.
          if(millis()-blinkLed>500){
            blinkLed = millis();
            led->toggle();
          }
          if(wantManual){
            manual = true;
            sc->out("0 1");
          } else {
            p = pSC;
            pSB = pSC;  // Per evitare scostamenti diga nel cambio di stato.
          }
        break;
        case 1:   // Manuale.
          led->turnOn();
          if(!wantManual){
            manual = false;
            sc->out("0 0");
          } else {
            if(pSB != p){
              p = pSB;
              pSC = pSB;  // Per evitare scostamenti diga nel cambio di stato.
              sc->out("1 " + String(p));
            }
          }
        break;
      }
    break;
  }

  dam->setOpening(p);
  
}
