#ifndef __SERIALCABLE__
#define __SERIALCABLE__
#include <Arduino.h>

class SerialCable {
public:
    SerialCable(int rate);
    void out(String msg);
    String in();
    bool present();
};

#endif
