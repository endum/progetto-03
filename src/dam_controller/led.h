#ifndef __LED__
#define __LED__
#include <Arduino.h>

class Led {
public:
    Led(int pin);
    void initDevice();
    bool turnOn();
    bool turnOff();
    bool toggle();
private:
    int pin;
    bool _on;
};

#endif
