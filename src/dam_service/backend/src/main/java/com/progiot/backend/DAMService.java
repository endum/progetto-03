package com.progiot.backend;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.Date;
import java.util.LinkedList;

/*
 * Data Service as a vertx event-loop 
 */
public class DAMService extends AbstractVerticle {

	private int port;	// Porta per server HTTP.
	private static final int MAX_SIZE = 20;	// Pool massimo di valori rilevati.
	private LinkedList<DataPoint> values;	// Valori rilevati.
	
	private static final String S_PORT = "COM5";	// Porta per SERIALE.
	private static final int RATE = 9600;	// Baudrate per SERIALE.
	private CommChannel scm;	// Canale seriale.
	
	private static final double D2 = 0.4;
	private static final double DELTA_D = 0.04;
	
	private int state = 0;		// Stato corrente diga.
	private int manual = 0;		// Manuale o automatico.
	private int opening = 0;	// Apertura calcolata diga.
	
	public DAMService(int port) throws Exception {
		scm = new SerialCommChannel(S_PORT, RATE);
		values = new LinkedList<>();		
		this.port = port;
	}

	public String stringState(int st) {
		switch(st) {
		case 0:
			return "NORMAL";
		case 1:
			return "PRE-ALLARM";
		case 2:
			return "ALLARM";
		}
		return "";
	}
	
	@Override
	public void start() {
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		router.post("/api/data").handler(this::handleAddNewData);
		router.get("/api/data").handler(this::handleGetData);
		vertx
			.createHttpServer()
			.requestHandler(router)
			.listen(port);

		new Thread() {
			public void run() {
				while(true) {
					try {
						String msg = scm.receiveMsg();
						log("ARRIVATO: " + msg);
						int code = Integer.valueOf(msg.substring(0,1));
						int val = Integer.valueOf(msg.substring(2));
						log("CODE:"+code+" val:"+val);
						switch(code) {
						case 0:
							manual = val;
							log("Switched to " + (manual == 1 ? "manual" : "automatic") + ".");
						break;
						case 1:
							opening = val;
							log("Setting DAM to " + opening + ".");
						break;
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
		
		scm.sendMsg("0 " + state);
		
		log("Service ready.");
	}
	
	private void handleAddNewData(RoutingContext rc) {
		HttpServerResponse response = rc.response();
		JsonObject res = rc.getBodyAsJson();
		if (res == null) {
			response.setStatusCode(400).end();
		} else if(res.containsKey("value")){
			float value = res.getFloat("value");
			String place = res.getString("place");
			long time = System.currentTimeMillis();
			
			values.addFirst(new DataPoint(value, time, place));
			if (values.size() > MAX_SIZE) {
				values.removeLast();
			}
			
			log("New value: " + value + " from " + place + " on " + new Date(time) + ".");
			
			if(manual == 0 && state == 2) {	// Se automatico e in allarme.
				int last = opening;
				if(value > D2) {
					opening = 0;
				} else if(value > D2 - DELTA_D) {
					opening = 20;
				} else if(value > D2 - 2 * DELTA_D) {
					opening = 40;
				} else if(value > D2 - 3 * DELTA_D) {
					opening = 60;
				} else if(value > D2 - 4 * DELTA_D) {
					opening = 80;
				} else {
					opening = 100;
				}
				
				if(last != opening) {
					scm.sendMsg("1 " + opening);
					log("Setting DAM to " + opening + ".");
				}
			}
			
			response.setStatusCode(200).end();
		} else if(res.containsKey("state")) {
			state = res.getInteger("state");
			String place = res.getString("place");
			
			scm.sendMsg("0 " + state);
			
			log("New state: " + stringState(state) + " from " + place + " on " + new Date(System.currentTimeMillis()) + ".");
			response.setStatusCode(200).end();
		}
	}
	
	private void handleGetData(RoutingContext rc) {
		JsonArray arr = new JsonArray();
		JsonObject jobj = new JsonObject();
		jobj.put("state", state);
		jobj.put("manual", manual);
		jobj.put("opening", opening);
		arr.add(jobj);
		for (DataPoint p: values) {
			JsonObject data = new JsonObject();
			data.put("time", p.getTime());
			data.put("value", p.getValue());
			data.put("place", p.getPlace());
			arr.add(data);
		}
		JsonObject obj = new JsonObject();
		obj.put("data", arr);
		rc.response()
			.putHeader("content-type", "application/json")
			.end(obj.encodePrettily());
	}
	
	private void log(String msg) {
		System.out.println("[DATA SERVICE] "+msg);
	}

	public static void main(String[] args) throws Exception {
		Vertx vertx = Vertx.vertx();
		DAMService service = new DAMService(8080);
		vertx.deployVerticle(service);
	}
}