package com.example.dammobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.dammobile.btlib.BluetoothChannel;
import com.example.dammobile.netutils.Http;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;

public class DAMActivity extends AppCompatActivity {

    private BluetoothChannel btc;
    private Handler httpHand;

    private static final double MAX_HIGH = 500;

    Runnable status = new Runnable() {
        @Override
        public void run() {
            try {
                tryHttpGet();
            } finally {
                httpHand.postDelayed(status, 200);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dam);

        btc = MainActivity.btChannel;
        httpHand = new Handler();

        initUI();

        status.run();
    }

    private void initUI(){
        findViewById(R.id.btn0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btc.sendMessage("0 1");
            }
        });

        ((Switch)findViewById(R.id.manualSwitch)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    buttonView.setEnabled(false);
                    btc.sendMessage("0 1");
                } else {
                    buttonView.setEnabled(false);
                    btc.sendMessage("0 0");
                    findViewById(R.id.btn0).setEnabled(false);
                    findViewById(R.id.btn20).setEnabled(false);
                    findViewById(R.id.btn40).setEnabled(false);
                    findViewById(R.id.btn60).setEnabled(false);
                    findViewById(R.id.btn80).setEnabled(false);
                    findViewById(R.id.btn100).setEnabled(false);
                }
            }
        });

        findViewById(R.id.btn0).setOnClickListener((v)->{
            btc.sendMessage("1 0");
        });
        findViewById(R.id.btn20).setOnClickListener((v)->{
            btc.sendMessage("1 20");
        });
        findViewById(R.id.btn40).setOnClickListener((v)->{
            btc.sendMessage("1 40");
        });
        findViewById(R.id.btn60).setOnClickListener((v)->{
            btc.sendMessage("1 60");
        });
        findViewById(R.id.btn80).setOnClickListener((v)->{
            btc.sendMessage("1 80");
        });
        findViewById(R.id.btn100).setOnClickListener((v)->{
            btc.sendMessage("1 100");
        });
    }

    private String itost(int code){
        if(code == 0){
            return "NORMAL";
        }
        if(code == 1){
            return "PRE-ALLARM";
        }
        return "ALLARM";
    }

    private void tryHttpGet(){
        final String url = "http://192.168.0.191:8080/api/data";

        Http.get(url, response -> {
            try{
                if(response.code() == HttpURLConnection.HTTP_OK) {
                    JSONObject obj = new JSONObject(response.contentAsString());
                    JSONArray arr = obj.getJSONArray("data");

                    JSONObject status = arr.getJSONObject(0);
                    JSONObject lastMes = arr.getJSONObject(1);

                    TextView state = findViewById(R.id.lblState);
                    TextView high = findViewById(R.id.lblHigh);
                    TextView open = findViewById(R.id.lblOpening);
                    ProgressBar water = findViewById(R.id.waterBar);
                    Switch manual = findViewById(R.id.manualSwitch);

                    state.setText(itost(status.getInt("state")));
                    water.setProgress((int)(MAX_HIGH - (lastMes.getDouble("value") * 100)));

                    switch (status.getInt("state")) {
                        case 0:
                            high.setText("");
                            open.setText("");
                            manual.setEnabled(false);
                            manual.setChecked(false);
                            findViewById(R.id.btn0).setEnabled(false);
                            findViewById(R.id.btn20).setEnabled(false);
                            findViewById(R.id.btn40).setEnabled(false);
                            findViewById(R.id.btn60).setEnabled(false);
                            findViewById(R.id.btn80).setEnabled(false);
                            findViewById(R.id.btn100).setEnabled(false);
                            break;
                        case 1:
                            open.setText("");
                            manual.setEnabled(false);
                            manual.setChecked(false);
                            findViewById(R.id.btn0).setEnabled(false);
                            findViewById(R.id.btn20).setEnabled(false);
                            findViewById(R.id.btn40).setEnabled(false);
                            findViewById(R.id.btn60).setEnabled(false);
                            findViewById(R.id.btn80).setEnabled(false);
                            findViewById(R.id.btn100).setEnabled(false);

                            high.setText(String.valueOf((int)(MAX_HIGH - (lastMes.getDouble("value") * 100))));
                            break;
                        case 2:
                            open.setText(status.getInt("opening") + "%");
                            high.setText(String.valueOf((int)(MAX_HIGH - (lastMes.getDouble("value") * 100))));
                            switch (status.getInt("manual")){
                                case 0:
                                    findViewById(R.id.btn0).setEnabled(false);
                                    findViewById(R.id.btn20).setEnabled(false);
                                    findViewById(R.id.btn40).setEnabled(false);
                                    findViewById(R.id.btn60).setEnabled(false);
                                    findViewById(R.id.btn80).setEnabled(false);
                                    findViewById(R.id.btn100).setEnabled(false);
                                    if(!manual.isChecked()){
                                        manual.setEnabled(true);
                                    }
                                    break;
                                case 1:
                                    findViewById(R.id.btn0).setEnabled(true);
                                    findViewById(R.id.btn20).setEnabled(true);
                                    findViewById(R.id.btn40).setEnabled(true);
                                    findViewById(R.id.btn60).setEnabled(true);
                                    findViewById(R.id.btn80).setEnabled(true);
                                    findViewById(R.id.btn100).setEnabled(true);
                                    if(manual.isChecked()){
                                        manual.setEnabled(true);
                                    }
                                    break;
                            }
                    }
                }
            } catch (Exception ex){

            }

        });
    }
}