#include "sonar.h"
#include "led.h"
#include "toServer.h"

// Da dove sto inviando dati.
#define PLACE "Diga"

// Stati del sistema.
#define NORMALE 0
#define PREALLARME 1
#define ALLARME 2

// Distanze dal sensore.
#define D1 1.00
#define D2 0.40

// Frequenze invio dati.
#define FREQ1 10
#define FREQ2 5

// Pin sull'ESP.
#define ECHO 4  // D2
#define TRIG 5  // D1
#define LED 16  // D0

int state;
int lastState;
unsigned long lastTime;
int ticks;

// Wi-Fi SSID.
char* ssidName = "Benve";
// WPA2 Wi-Fi password.
char* pwd = "banagola";
// DAM Service indirizzo IP. 
char* address = "http://192.168.0.191:8080";

Sonar* son;
Led* led;
ToServer* toS;

void setup() {
  son = new Sonar(ECHO, TRIG);
  son->initDevice();

  led = new Led(LED);
  led->initDevice();

  toS = new ToServer(address, PLACE);
  toS->startWiFi(ssidName, pwd);

  lastTime = millis();
  lastState = NORMALE;
  ticks = 0;
}

void tick(){
  // Lettura sonar.
  float value = son->getDistance();

  // Determinazione stato.
  if(value > D1){
    state = NORMALE;
  } else if (value > D2){
    state = PREALLARME;
  } else {
    state = ALLARME;
  }

  // Se lo stato è cambiato informo il server.
  if(state != lastState){
    lastState = state;
    ticks = 0;
    toS->sendState(state);

    switch(state){
      case NORMALE:
        led->turnOff();
      break;
      case ALLARME:
        led->turnOn();
      break;
    }
  }

  switch(state){
    case PREALLARME:
      led->toggle();
      if(!(ticks % FREQ1)){
        ticks = 0;
        toS->sendValue(value);
      }
    break;
    case ALLARME:
      if(!(ticks % FREQ2)){
        ticks = 0;
        toS->sendValue(value);
      }
    break;
  }
  
  ticks++;
}

void loop() { 
  if(millis() - lastTime > 1000){
    lastTime = millis();
    tick();
  }
}
