#ifndef __TOSERVER__
#define __TOSERVER__
#include <Arduino.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

class ToServer {
public:
    ToServer(String address, String place);
    void startWiFi(char* ssidName, char* pwd);
    int sendValue(float value);
    int sendState(int state);
private:
    String address;
    String place;
};

#endif
