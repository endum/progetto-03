#include <Arduino.h>
#include "Led.h"

Led::Led(int pin){
  this->pin = pin;
  this->_on = 0;
}

bool Led::turnOn(){
  this->_on = 1;
  digitalWrite(this->pin, HIGH);
}
bool Led::turnOff(){
  this->_on = 0;
  digitalWrite(this->pin, LOW);
}
bool Led::toggle(){  // Se acceso spengo, se spento accendo.
  this->_on = !this->_on;
  digitalWrite(this->pin, this->_on ? HIGH : LOW);
}

void Led::initDevice(){
  pinMode(pin, OUTPUT);
}
