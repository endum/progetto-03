#include <Arduino.h>
#include "toServer.h"

ToServer::ToServer(String address, String place){
  this->address = address;
  this->place = place;
}

void ToServer::startWiFi(char* ssidName, char* pwd){
  WiFi.begin(ssidName, pwd);
  while (WiFi.status() != WL_CONNECTED) {  
    delay(100);
  }
}
int ToServer::sendValue(float value){
  if(WiFi.status() != WL_CONNECTED){
    return 400;
  }
  HTTPClient http;
  
  http.begin(this->address + "/api/data");      
  http.addHeader("Content-Type", "application/json");
  int retCode = http.POST(String("{ \"value\": ") + String(value) + ", \"place\": \"" + place + "\" }");
  http.end();    
  return retCode;
}

int ToServer::sendState(int state){
  if(WiFi.status() != WL_CONNECTED){
    return 400;
  }
  HTTPClient http;
  
  http.begin(this->address + "/api/data");      
  http.addHeader("Content-Type", "application/json");
  int retCode = http.POST(String("{ \"state\": ") + String(state) + ", \"place\": \"" + place + "\" }");
  
  http.end();    
  return retCode;
}
